import { Injectable }       from '@angular/core';
import { CanActivate } from '@angular/router';

import { AuthService } from '../../shared/services';

@Injectable()
export class NoAuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
  ) {}

  public canActivate(): boolean {
    return !this.authService.isLoggedIn();
  }
}
