import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../../../shared';

@Component({
  selector: 'gh-auth',
  templateUrl: './auth.component.pug',
  styleUrls: ['./auth.component.styl'],
})
export class AuthComponent {
  public accessToken = new FormControl('', [
    Validators.required,
    Validators.pattern(/^\w+$/i),
  ]);

  constructor(
    private authService: AuthService,
    private router: Router,
  ) { }

  public login(): void {
    this.authService.login(this.accessToken.value);

    this.redirect();
  }

  public loginWithDefault(): void {
    this.authService.login();

    this.redirect();
  }

  private redirect(): void {
    const redirectUrl = this.authService.redirectUrl ? this.authService.redirectUrl : '/users';

    this.router.navigate([redirectUrl]);
    this.authService.redirectUrl = '';
  }
}
