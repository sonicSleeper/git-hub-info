import { NgModule } from '@angular/core';
import { RouterModule, Routes  } from '@angular/router';

import { SharedModule } from '../shared';

import { AuthComponent } from './components/auth/auth.component';
import { NoAuthGuard } from './services/no-auth-guard.service';

const routes: Routes = [
  {
    path: 'authorization',
    component: AuthComponent,
    canActivate: [NoAuthGuard],
    data: { depth: 1 },
  },
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
  ],
  declarations: [AuthComponent],
  providers: [NoAuthGuard],
})
export class AuthModule { }
