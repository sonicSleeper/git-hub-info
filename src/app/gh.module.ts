import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AuthModule } from './auth/auth.module';
import { GhRoutingModule } from './gh-routing.module';
import { GhComponent } from './gh.component';
import { RepositoryModule } from './repository/repository.module';
import {
  AuthGuard,
  AuthService,
  BranchesAPIService,
  BranchesService,
  CommitsAPIService,
  CommitsService,
  DataCleanerService,
  ErrorHandlerService,
  ErrorPopupService,
  RepositoriesAPIService,
  RepositoriesService,
  SharedModule,
  TokenInterceptor,
  TokensService,
  UsersAPIService,
  UsersService,
} from './shared';
import { UsersModule } from './users/users.module';

@NgModule({
  declarations: [
    GhComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    UsersModule,
    AuthModule,
    RepositoryModule,
    GhRoutingModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    UsersService,
    RepositoriesService,
    BranchesService,
    CommitsService,
    ErrorHandlerService,
    ErrorPopupService,
    AuthService,
    DataCleanerService,
    AuthGuard,
    TokensService,
    UsersAPIService,
    RepositoriesAPIService,
    CommitsAPIService,
    BranchesAPIService,
  ],
  bootstrap: [GhComponent],
})
export class GhModule { }
