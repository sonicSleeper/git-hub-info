import { Component } from '@angular/core';

@Component({
  selector: 'gh-content-loading',
  templateUrl: './content-loading.component.pug',
  styleUrls: ['./content-loading.component.styl'],
})
export class ContentLoadingComponent { }
