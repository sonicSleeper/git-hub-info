import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'gh-error-message',
  templateUrl: './error-message.component.pug',
  styleUrls: ['./error-message.component.styl'],
})
export class ErrorMessageComponent {
  constructor(
    public dialogRef: MatDialogRef<ErrorMessageComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  public close(): void {
    this.dialogRef.close();
  }
}
