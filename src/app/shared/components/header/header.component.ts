import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'gh-header',
  templateUrl: './header.component.pug',
  styleUrls: ['./header.component.styl'],
})
export class HeaderComponent {
  constructor(
    public authService: AuthService,
    public router: Router,
    private location: Location,
  ) {}

  public back(): void {
    this.location.back();
  }

  public logout(): void {
    this.authService.logout();

    this.router.navigate(['/authorization']);
  }
}
