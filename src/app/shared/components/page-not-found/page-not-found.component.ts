import { Component, Input } from '@angular/core';

@Component({
  selector: 'gh-page-not-found',
  templateUrl: './page-not-found.component.pug',
  styleUrls: ['./page-not-found.component.styl'],
})
export class PageNotFoundComponent {
  @Input() public navigationUrl: string = '/users';
  @Input() public linkLabel: string = 'Back to users list';
  @Input() public headerText: string = '404 Page not found';
}
