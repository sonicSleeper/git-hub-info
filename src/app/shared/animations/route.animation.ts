import {
  AnimationTriggerMetadata,
  animate,
  group,
  query,
  style,
  transition,
  trigger,
} from '@angular/animations';

export const routeAnimation: AnimationTriggerMetadata =
  trigger('routeAnimation', [
    transition('1 => 2, 2 => 3, 3 => 4', [
      style({ height: '!' }),
      query(':enter', style({ transform: 'translateX(100%)' })),
      query(':enter, :leave', style({ position: 'absolute', top: '64px', left: 0, right: 0 })),
      group([
        query(':leave', [
          animate('.4s cubic-bezier(.35,0,.25,1)', style({ transform: 'translateX(-100%)' })),
        ]),
        query(':enter', animate('.4s cubic-bezier(.35, 0, .25, 1)')),
      ]),
    ]),
    transition('4 => 3, 3 => 2, 4 => 1, 3 => 1, 2 => 1', [
      style({ height: '!' }),
      query(':enter', style({ transform: 'translateX(-100%)' })),
      query(':enter, :leave', style({ position: 'absolute', top: '64px', left: 0, right: 0 })),
      group([
        query(':leave', [
          style({ transform: 'translateX(15%)' }),
          animate('.4s cubic-bezier(.35,0,.25,1)', style({ transform: 'translateX(100%)' })),
        ]),
        query(':enter', animate('.4s cubic-bezier(.35, 0, .25, 1)')),
      ]),
    ]),
  ]);
