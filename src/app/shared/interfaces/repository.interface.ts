export interface IRepository {
  id: number;
  name: string;
  full_name: string;
  description: string;
  default_branch: string;
  owner: {
    login: string;
  };
}
