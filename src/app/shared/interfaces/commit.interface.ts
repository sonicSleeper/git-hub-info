export interface ICommit {
  commit: {
    message: string,
    committer: {
      name: string,
      date: string,
      email: string,
    },
  };
}
