import { Injectable } from '@angular/core';

import { BranchesService } from './branches.service';
import { CommitsService } from './commits.service';
import { RepositoriesService } from './repositories.service';
import { TokensService } from './tokens.service';
import { UsersService } from './users.service';

@Injectable()
export class DataCleanerService {
  constructor(
    private tokensService: TokensService,
    private usersService: UsersService,
    private branchesService: BranchesService,
    private reposService: RepositoriesService,
    private commitsService: CommitsService,
  ) {}

  public cleanAll(): void {
    this.tokensService.remove();
    this.usersService.users = [];
    this.usersService.currentUser = null;
    this.commitsService.commits = [];
    this.branchesService.branches = [];
    this.reposService.repositories = [];
    this.reposService.currentRepository = null;
  }

  public cleanBranchesCommits(): void {
    this.commitsService.commits = [];
    this.branchesService.branches = [];
  }
}
