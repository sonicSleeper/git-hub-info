import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { catchError, map } from 'rxjs/operators';

import { IUser } from '../interfaces';

import { UsersAPIService } from './api';
import { ErrorHandlerService } from './error-handler.service';

@Injectable()
export class UsersService {
  public users: IUser[] = [];
  public currentUser: IUser;

  public loadingUsers: boolean = false;
  public loadingUser: boolean = false;

  constructor(
    private errorHandler: ErrorHandlerService,
    private apiService: UsersAPIService,
  ) {}

  public getUsers(): Observable<IUser[]> {
    this.loadingUsers = true;

    if (this.users.length) {
      return Observable.of(this.users);
    }

    return this.apiService.getUsers()
      .pipe(
        map((users) => {
          this.users = this.users.concat(users);
          this.loadingUsers = false;
          return users;
        }),
        catchError((error) => {
          this.loadingUsers = false;
          return this.errorHandler.handleError(error);
        }),
      );
  }

  public getUser(login: string): Observable<IUser> {
    this.loadingUser = true;

    if (this.currentUser && this.currentUser.login !== login) {
      this.setCurrentUser(null);
    }

    return this.apiService.getUser(login)
      .pipe(
        map((user) => {
          this.setCurrentUser(user);
          this.loadingUser = false;
          return user;
        }),
        catchError((error) => {
          this.loadingUser = false;
          return this.errorHandler.handleError(error);
        }),
      );
  }

  public setCurrentUser(user: IUser): void {
    this.currentUser = user;
  }
}
