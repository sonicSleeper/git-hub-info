import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Rx';

import { IBranch } from '../../interfaces';

import { environment } from '../../../../environments/environment';

@Injectable()
export class BranchesAPIService {
  constructor(private http: HttpClient) {}

  public get(login:string, repoName:string): Observable<IBranch[]> {
    const url = `${environment.API_HOST}/repos/${login}/${repoName}/branches`;

    return this.http.get<IBranch[]>(url);
  }
}
