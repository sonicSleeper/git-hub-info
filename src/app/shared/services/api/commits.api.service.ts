import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Rx';

import { ICommit } from '../../interfaces';

import { environment } from '../../../../environments/environment';

@Injectable()
export class CommitsAPIService {
  constructor(private http: HttpClient) {}

  public get(login: string, repository: string): Observable<ICommit[]> {
    const url = `${environment.API_HOST}/repos/${login}/${repository}/commits`;

    return this.http.get<ICommit[]>(url);
  }
}
