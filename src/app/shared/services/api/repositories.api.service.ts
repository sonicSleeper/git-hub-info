import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Rx';

import { IRepository } from '../../interfaces';

import { environment } from '../../../../environments/environment';

@Injectable()
export class RepositoriesAPIService {
  constructor(private http: HttpClient) {}

  public getRepositories(login: string): Observable<IRepository[]> {
    const url = `${environment.API_HOST}/users/${login}/repos`;

    return this.http.get<IRepository[]>(url);
  }

  public getRerository(login: string, repository: string): Observable<IRepository> {
    const url =  `${environment.API_HOST}/repos/${login}/${repository}`;

    return this.http.get<IRepository>(url);
  }
}
