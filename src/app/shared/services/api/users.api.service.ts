import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Rx';

import { IUser } from '../../interfaces';

import { environment } from '../../../../environments/environment';

@Injectable()
export class UsersAPIService {
  constructor(private http: HttpClient) {}

  public getUsers(): Observable<IUser[]> {
    const url = `${environment.API_HOST}/users`;

    return this.http.get<IUser[]>(url);
  }

  public getUser(login: string): Observable<IUser> {
    const url = `${environment.API_HOST}/users/${login}`;

    return this.http.get<IUser>(url);
  }
}
