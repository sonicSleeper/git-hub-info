import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';

import { ErrorMessageComponent } from '../components/error-message/error-message.component';

@Injectable()
export class ErrorPopupService {
  constructor(
    private dialog: MatDialog,
  ) { }

  public openPopup(header: string, message: string): void {
    this.dialog.open(ErrorMessageComponent, {
      width: '320px',
      data: { header, message },
    });
  }
}
