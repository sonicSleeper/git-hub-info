import { Injectable } from '@angular/core';

@Injectable()
export class TokensService {
  private defaultKey: string = 'e77e872acf5b1f4b2f48d654ff313f47fa0f199e';

  public save(token?: string): void {
    if (token) {
      return localStorage.setItem('token', token);
    }

    localStorage.setItem('token', this.defaultKey);
  }

  public remove(): void {
    localStorage.removeItem('token');
  }

  public get(): string {
    return localStorage.getItem('token');
  }
}
