import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Rx';
import { catchError, map } from 'rxjs/operators';

import { IRepository } from '../interfaces';

import { RepositoriesAPIService } from './api';
import { ErrorHandlerService } from './error-handler.service';

@Injectable()
export class RepositoriesService {
  public currentRepository: IRepository;
  public repositories: IRepository[] = [];

  public loadingRepository: boolean = false;
  public loadingRepositories: boolean = false;

  constructor(
    private apiService: RepositoriesAPIService,
    private errorHandler: ErrorHandlerService,
  ) {}

  public getRepositories(login: string): Observable<IRepository[]> {
    this.loadingRepositories = true;

    if (this.repositories.length) {
      if (this.repositories[0].owner.login !== login) {
        this.repositories = [];
      }
    }

    return this.apiService.getRepositories(login)
      .pipe(
        map((repositories) => {
          this.repositories = repositories;
          this.loadingRepositories = false;
          return repositories;
        }),
        catchError((error) => {
          this.loadingRepositories = false;
          return this.errorHandler.handleError(error);
        }),
      );
  }

  public getRepository(login: string, repoName: string): Observable<IRepository> {
    this.loadingRepository = true;

    if (this.repositories.length) {
      const repository = this.repositories.find(element => element.name === repoName);

      if (repository) {
        this.loadingRepository = false;
        this.currentRepository = repository;
        return Observable.of(repository);
      }
    }

    return this.apiService.getRerository(login, repoName)
      .pipe(
        map((repository) => {
          this.currentRepository = repository;
          this.loadingRepository = false;
          return repository;
        }),
        catchError((error) => {
          this.loadingRepository = false;
          return this.errorHandler.handleError(error);
        }),
      );
  }

  public cleanSavedData(): void {
    this.currentRepository = null;
    this.repositories = [];
  }
}
