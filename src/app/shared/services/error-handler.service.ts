import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

import { ErrorPopupService } from './error-popup.service';
import { TokensService } from './tokens.service';

@Injectable()
export class ErrorHandlerService {
  constructor(
    private errorPopup: ErrorPopupService,
    private tokensService: TokensService,
    private router: Router,
  ) { }

  public handleError(error: HttpErrorResponse): ErrorObservable  {
    if (error.error instanceof ErrorEvent) {

    } else {
      switch (error.status) {
        case 0:
          this.throwPopup(
            'Cannot connect to remote server.',
            'Please, check your local network or contact your internet provider.',
          );
          break;
        case 400:
          this.throwPopup(
            'Bad request',
            'Please, check information before sending.',
          );
          break;
        case 401:
          this.tokensService.remove();
          this.router.navigate(['/authorization']);

          this.throwPopup(
            'Wrong credentials',
            'Remote server did not accept your access token. Please, create another or use the default one.',
          );
          break;
        case 404:
          break;
        case 409:
          this.throwPopup(
            'Conflict',
            'It seems like this repository is empty',
          );
          break;
        case 500:
          this.throwPopup(
            'Remote server error',
            'Please, try to reload page and repeat your action. If it does not help, contact GitHub support team ' +
            'https://github.com/support',
          );
          break;
        default:
          this.throwPopup(
            'Oops!',
            'Something went wrong.',
          );
          break;
      }
    }
    throw new ErrorObservable(error);
  }

  private throwPopup(headline: string, message: string): void {
    this.errorPopup.openPopup(headline, message);
  }
}
