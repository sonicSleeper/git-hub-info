import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { catchError, map } from 'rxjs/operators';

import { IBranch } from '../interfaces';

import { BranchesAPIService } from './api';
import { ErrorHandlerService } from './error-handler.service';

@Injectable()
export class BranchesService {
  public branches: IBranch[] = [];
  public loading: boolean = false;

  constructor(
    private apiService: BranchesAPIService,
    private errorHandler: ErrorHandlerService,
  ) {}

  public getBranches(login:string, repoName:string): Observable<IBranch[]> {
    this.loading = true;

    if (this.branches.length) {
      this.loading = false;
      return Observable.of(this.branches);
    }

    return this.apiService.get(login, repoName)
      .pipe(
        map((branches) => {
          this.branches = branches;
          this.loading = false;
          return branches;
        }),
        catchError((error) => {
          this.loading = false;
          return this.errorHandler.handleError(error);
        }),
      );
  }
}
