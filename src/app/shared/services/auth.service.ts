import { Injectable } from '@angular/core';

import { DataCleanerService } from './data-cleaner.service';
import { TokensService } from './tokens.service';

@Injectable()
export class AuthService {
  public redirectUrl: string;

  constructor(
    private dataCleanerService: DataCleanerService,
    private tokensService: TokensService,
  ) {}

  public login(token?: string): void {
    this.tokensService.save(token);
  }

  public logout(): void {
    this.dataCleanerService.cleanAll();
  }

  public isLoggedIn(): boolean {
    return !!this.tokensService.get();
  }
}
