import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Rx';
import { catchError, map } from 'rxjs/operators';

import { ICommit } from '../interfaces';

import { CommitsAPIService } from './api';
import { ErrorHandlerService } from './error-handler.service';

@Injectable()
export class CommitsService {
  public commits: ICommit[] = [];
  public loading: boolean = false;

  constructor(
    private apiService: CommitsAPIService,
    private errorHandler: ErrorHandlerService,
  ) {}

  public getCommits(login: string, repository: string): Observable<ICommit[]> {
    this.loading = true;

    if (this.commits.length) {
      this.loading = false;
      return Observable.of(this.commits);
    }

    return this.apiService.get(login, repository)
      .pipe(
        map((commits) => {
          this.commits = commits;
          this.loading = false;
          return commits;
        }),
        catchError((error) => {
          this.loading = false;
          return this.errorHandler.handleError(error);
        }),
      );
  }
}
