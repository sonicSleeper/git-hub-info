import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatChipsModule,
  MatDialogModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatProgressSpinnerModule,
  MatTabsModule,
  MatToolbarModule,
} from '@angular/material';

import { RouterModule } from '@angular/router';

import {
  ContentLoadingComponent,
  ErrorMessageComponent,
  HeaderComponent,
  PageNotFoundComponent,
} from './components';

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    MatButtonModule,
    MatChipsModule,
    MatDialogModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatTabsModule,
    MatToolbarModule,
  ],
  declarations: [
    ContentLoadingComponent,
    ErrorMessageComponent,
    PageNotFoundComponent,
    HeaderComponent,
  ],
  entryComponents: [
    ErrorMessageComponent,
  ],
  exports: [
    MatProgressSpinnerModule,
    MatToolbarModule,
    MatButtonModule,
    MatInputModule,
    MatListModule,
    MatChipsModule,
    MatIconModule,
    MatExpansionModule,
    MatTabsModule,
    MatDialogModule,
    MatFormFieldModule,
    ContentLoadingComponent,
    ErrorMessageComponent,
    HeaderComponent,
    PageNotFoundComponent,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
  ],
})
export class SharedModule { }
