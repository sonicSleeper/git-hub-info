import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

import { routeAnimation } from './shared';

@Component({
  selector: 'gh-root',
  templateUrl: './gh.component.pug',
  styleUrls: ['./gh.component.styl'],
  animations: [routeAnimation],
})
export class GhComponent {
  public getDepth(outlet: RouterOutlet): void {
    return outlet.activatedRouteData['depth'];
  }
}
