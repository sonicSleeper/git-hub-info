import { NgModule } from '@angular/core';
import { RouterModule, Routes  } from '@angular/router';

import { AuthGuard, SharedModule } from '../shared';

import { BranchesComponent, CommitsComponent, RepositoryComponent } from './components';

const routes: Routes = [
  {
    path: 'users/:login/:repository',
    component: RepositoryComponent,
    data: { depth: 4 },
    canActivate: [AuthGuard],
    children:[
      {
        path: '',
        component: BranchesComponent,
        canActivateChild: [AuthGuard],
      },
      {
        path: 'commits',
        component: CommitsComponent,
        canActivateChild: [AuthGuard],
      },
    ],
  },
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    RepositoryComponent,
    CommitsComponent,
    BranchesComponent,
  ],
})
export class RepositoryModule { }
