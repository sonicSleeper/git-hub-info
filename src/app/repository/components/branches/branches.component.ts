import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';

import { Observable } from 'rxjs/Rx';

import { BranchesService, IBranch, RepositoriesService } from '../../../shared';

@Component({
  selector: 'gh-branches',
  templateUrl: './branches.component.pug',
  styleUrls: ['./branches.component.styl'],
})
export class BranchesComponent implements OnInit {
  public branches: IBranch[] = [];

  constructor(
    public reposService: RepositoriesService,
    public branchesService: BranchesService,
    private route: ActivatedRoute,
  ) { }

  public ngOnInit(): void {
    this.loadBranches().subscribe(branches => this.branches = branches);
  }

  private loadBranches(): Observable<IBranch[]> {
    return this.route.paramMap.switchMap((params: ParamMap) => {
      const login = params.get('login');
      const repoName = params.get('repository');

      return this.branchesService.getBranches(login, repoName);
    });
  }
}
