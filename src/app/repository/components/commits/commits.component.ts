import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs/Rx';

import { CommitsService, ICommit } from '../../../shared';

@Component({
  selector: 'gh-commits',
  templateUrl: './commits.component.pug',
  styleUrls: ['./commits.component.styl'],
})
export class CommitsComponent implements OnInit {
  public commits: ICommit[] = [];

  constructor(
    public commitsService: CommitsService,
    private route: ActivatedRoute,
  ) { }

  public ngOnInit(): void {
    this.loadCommits().subscribe(commits => this.commits = commits);
  }

  private loadCommits(): Observable<ICommit[]> {
    const loginParam = this.route.parent.snapshot.params.login;
    const repositoryParam = this.route.parent.snapshot.params.repository;

    return this.commitsService.getCommits(loginParam, repositoryParam);
  }
}
