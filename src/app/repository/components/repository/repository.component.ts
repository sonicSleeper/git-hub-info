import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import {
  DataCleanerService,
  IRepository,
  RepositoriesService,
} from '../../../shared';

@Component({
  selector: 'gh-repository',
  templateUrl: './repository.component.pug',
  styleUrls: ['./repository.component.styl'],
})
export class RepositoryComponent implements OnInit, OnDestroy {
  public repository: IRepository;
  public navLinks: { path: string[], label: string, icon: string }[];
  public selectedRoute = 'Branches';

  public notFoundError: {
    navigationUrl: string,
    linkLabel: string,
    headerText: string,
  };

  private login: string;

  constructor(
    public repositoriesService: RepositoriesService,
    private route: ActivatedRoute,
    private router: Router,
    private dataCleanerService: DataCleanerService,
  ) { }

  public ngOnInit(): void {
    document.documentElement.scrollTop = 0;

    this.loadRepository().subscribe(
      (repository) => {
        this.repository = repository;

        this.navLinks = [
          {
            path: ['/users', this.repository.owner.login, this.repository.name],
            label: 'Branches',
            icon: 'mdi mdi-source-branch',
          },
          {
            path: ['/users', this.repository.owner.login, this.repository.name, 'commits'],
            label: 'Commits',
            icon: 'mdi mdi-history',
          },
        ];
      },
      (error) => {
        if (error.error.status === 404) {
          this.notFoundError = {
            navigationUrl: `/users/${this.login}`,
            linkLabel: `Back to ${this.login} profile`,
            headerText: `Cannot find such repository`,
          };
        }
      });
  }

  public changeRoute(path: string[], routeName: string): void {
    this.selectedRoute = routeName;
    this.router.navigate(path, { replaceUrl: true });
  }

  public ngOnDestroy(): void {
    this.dataCleanerService.cleanBranchesCommits();
  }

  private loadRepository(): Observable<IRepository> {
    return this.route.paramMap.switchMap((params: ParamMap) => {
      this.login = params.get('login');
      const repoName = params.get('repository');

      return this.repositoriesService.getRepository(this.login, repoName);
    });
  }
}
