import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import {
  UsersService,
} from '../../../shared';

@Component({
  selector: 'gh-users-list',
  templateUrl: './users-list.component.pug',
  styleUrls: ['./users-list.component.styl'],
})
export class UsersListComponent implements OnInit {
  constructor(
    public usersService: UsersService,
    private router: Router,
  ) { }

  public ngOnInit(): void {
    document.documentElement.scrollTop = 0;

    this.usersService.getUsers().subscribe();
  }

  public onLinkClick(user): void {
    this.usersService.setCurrentUser(user);

    this.router.navigate(['/users', user.login]);
  }
}
