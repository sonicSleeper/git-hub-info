import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';

import { Observable } from 'rxjs/Rx';
import { forkJoin } from 'rxjs/observable/forkJoin';

import {
  IRepository,
  IUser,
  RepositoriesService,
  UsersService,
} from '../../../shared';

@Component({
  selector: 'gh-user-details',
  templateUrl: './user-details.component.pug',
  styleUrls: ['./user-details.component.styl'],
})
export class UserDetailsComponent implements OnInit {
  public notFoundError: string;

  constructor(
    public usersService: UsersService,
    public reposService: RepositoriesService,
    private route: ActivatedRoute,
  ) { }

  public ngOnInit(): void {
    document.documentElement.scrollTop = 0;

    this.loadData().subscribe(null, (error) => {
      if (error.error.status === 404) {
        this.notFoundError = 'Cannot find such user';
      }
    });
  }

  public get user(): IUser {
    return this.usersService.currentUser;
  }

  public get loadingUser(): boolean {
    return this.usersService.loadingUser;
  }

  public get repositories(): IRepository[] {
    return this.reposService.repositories;
  }

  public get loadingRepos(): boolean {
    return this.reposService.loadingRepositories;
  }

  private loadData(): Observable<[IRepository[], IUser]> {
    return this.route.paramMap.switchMap((params: ParamMap) => {
      return forkJoin(
        this.reposService.getRepositories(params.get('login')),
        this.usersService.getUser(params.get('login')));
    });
  }
}
