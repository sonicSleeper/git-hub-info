import { NgModule } from '@angular/core';
import { RouterModule, Routes  } from '@angular/router';

import { AuthGuard, SharedModule } from '../shared';

import { UserDetailsComponent, UsersListComponent } from './components';

const routes: Routes = [
  {
    path: 'users',
    component: UsersListComponent,
    canActivate: [AuthGuard],
    data: { depth: 2 },
  },
  {
    path: 'users/:login',
    component: UserDetailsComponent,
    canActivate: [AuthGuard],
    data: { depth: 3 },
  },
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    UsersListComponent,
    UserDetailsComponent,
  ],
})
export class UsersModule { }
